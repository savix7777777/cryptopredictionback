const mongoose = require("mongoose");
const cron = require("node-cron");
const { mainConnection } = require('../db');
const { loadPrediction } = require("./loadingFunction");
const { intervals } = require("./intervals");
const { cryptoCoinSchema } = require("../models/CryptoCoin");
const { prediction1D } = require("./jobs/prediction1D");
const { prediction7D } = require("./jobs/prediction7D");
const { prediction30D } = require("./jobs/prediction30D");
const { histodata } = require("./jobs/histodata");
const { sleep } = require("../utils/sleep");

const runCronJobs = async () => {
    try {
        mainConnection.on('connected', async () => {
            cron.schedule(intervals.day, async () => {
                try {
                    const collections = await mainConnection.db.listCollections().toArray();
                    for (let collection of collections) {
                        const collectionName = collection.name;
                        const Crypto = mongoose.model(collectionName, cryptoCoinSchema, collectionName);
                        const doc = await Crypto.findOne({}, 'symbol');
                        if (doc && doc.symbol) {
                            await loadPrediction("Prediction1D.py", prediction1D, collectionName, doc.symbol);
                            await sleep(200);
                            await loadPrediction("Prediction7D.py", prediction7D, collectionName, doc.symbol);
                            await sleep(200);
                            await loadPrediction("Prediction30D.py", prediction30D, collectionName, doc.symbol);
                            await sleep(200);
                            await histodata(doc.symbol, 'USD', 6, 'histoday', collectionName);
                            await sleep(200);
                            await histodata(doc.symbol, 'USD', 48, 'histoday', collectionName);
                            await sleep(200);
                            await histodata(doc.symbol, 'USD', 168, 'histohour', collectionName);
                            await sleep(200);
                            await histodata(doc.symbol, 'USD', 209, 'histoday', collectionName);
                            await sleep(200);
                        }
                    }
                } catch (err) {
                    console.error(err)
                }
            }, {
                scheduled: true,
                timezone: "UTC"
            });
        });
    } catch (error) {
        console.error('Error with running cron job:', error);
    }
}


module.exports = { runCronJobs };
