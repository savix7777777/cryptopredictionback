const intervals = {
    day: "10 0 * * *",
    week: "10 0 * * 0",
    month: "10 0 1 * *",
    test: "* * * * *"
}

module.exports = { intervals };
