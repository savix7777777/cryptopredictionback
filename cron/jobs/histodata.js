const mongoose = require("mongoose");
const axios = require('axios');
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

const histodata = async (
    fromSymbol,
    toSymbol,
    period,
    type,
    name
) => {
    try {
        let accountState = 0;
        const fetchDate = async () => {
            const url = `https://min-api.cryptocompare.com/data/v2/${type}?fsym=${fromSymbol}&tsym=${toSymbol}&limit=${period}&api_key=${
                accountState ? process.env.CRYPTO_COMPARE_KEY_1 : process.env.CRYPTO_COMPARE_KEY_0
            }`;

            const response = await axios.get(url, {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
            });

            if (response && response.data && response.data.Data) {
                const histoDate = response.data.Data.Data.map((item) => item.close);
                const model = mongoose.model(name, cryptoCoinSchema, name);

                const field = `histoDate${period}`;

                const result = await model.findOneAndUpdate(
                    { },
                    {
                        $set: {
                            [field]: histoDate,
                        },
                    },
                    { new: true }
                );

                console.log(name + " histoDate updated successfully", result);
            } else {
                if (!accountState) {
                    accountState = 1;
                    await fetchDate();
                }
                console.error('Error during getting histoDate');
            }
        }

        try {
            await fetchDate();
        } catch (error) {
            if (!accountState) {
                accountState = 1;
                await fetchDate();
            }
            console.error('Error during getting histoDate:', error);
        }
    } catch (error) {
        console.log("Error updating histoDate: ", error);
    }
}

module.exports = { histodata };