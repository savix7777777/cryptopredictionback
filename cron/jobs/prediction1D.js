const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

const prediction1D = async (prediction, accuracy, name) => {
    try {
        const model = mongoose.model(name, cryptoCoinSchema, name);

        const result = await model.findOneAndUpdate(
            { },
            {
                $set: {
                    'data1D.price': prediction,
                    'data1D.accuracy': accuracy
                },
                $push: { 'data1D.predictionsHistory': prediction }
            },
            { new: true }
        );

        if (result.data1D.predictionsHistory.length >= 8) {
            await model.findOneAndUpdate(
                { },
                { $pop: { 'data1D.predictionsHistory': -1 } }
            );
        }

        console.log(name + " 1D prediction updated successfully", result);
    } catch (error) {
        console.log("Error updating 1D prediction: ", error);
    }
}

module.exports = { prediction1D };
