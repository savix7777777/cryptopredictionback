const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

const prediction30D = async (prediction, accuracy, name) => {
    try {
        const model = mongoose.model(name, cryptoCoinSchema, name);

        const result = await model.findOneAndUpdate(
            { },
            {
                $set: {
                    'data30D.price': prediction,
                    'data30D.accuracy': accuracy
                },
                $push: { 'data30D.predictionsHistory': prediction }
            },
            { new: true }
        );

        if (result.data30D.predictionsHistory.length >= 280) {
            await model.findOneAndUpdate(
                { },
                { $pop: { 'data30D.predictionsHistory': -1 } }
            );
        }

        console.log(name + " 30D prediction updated successfully", result);
    } catch (error) {
        console.log("Error updating 30D predict: ", error);
    }
}

module.exports = { prediction30D };
