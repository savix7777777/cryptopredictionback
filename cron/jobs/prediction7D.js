const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

const prediction7D = async (prediction, accuracy, name) => {
    try {
        const model = mongoose.model(name, cryptoCoinSchema, name);

        const result = await model.findOneAndUpdate(
            { },
            {
                $set: {
                    'data7D.price': prediction,
                    'data7D.accuracy': accuracy
                },
                $push: { 'data7D.predictionsHistory': prediction }
            },
            { new: true }
        );

        if (result.data7D.predictionsHistory.length >= 56) {
            await model.findOneAndUpdate(
                { },
                { $pop: { 'data7D.predictionsHistory': -1 } }
            );
        }

        console.log(name + " 7D prediction updated successfully", result);
    } catch (error) {
        console.log("Error updating 7D prediction: ", error);
    }
}

module.exports = { prediction7D };
