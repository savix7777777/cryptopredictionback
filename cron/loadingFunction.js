const { PythonShell } = require("python-shell");

const loadPrediction = async (executableFile, updateDB, name, symbol) => {
    let options = {
        mode: 'text',
        pythonOptions: ['-u'],
        cwd: process.env.MODE === 'dev' ? '../CryptoPredictorML' : '/home/ubuntu/cryptoprediction',
        pythonPath: process.env.MODE === 'dev' ? 'venv/bin/python3' : '/home/ubuntu/cryptoprediction/venv/bin/python3',
        args: [symbol]
    };

    try {
        const result =  await PythonShell.run(executableFile, options);
        updateDB(result[0], result[1], name);
    } catch (err) {
        console.error(err)
    }
}

module.exports = { loadPrediction }
