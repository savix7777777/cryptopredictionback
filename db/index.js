const mongoose = require('mongoose').default;
const { mongoURL, mongoURLRoadmap, mongoURLUsers } = require("../config");

mongoose.connect(mongoURL).then(() => {});

const roadmapConnection = mongoose.createConnection(mongoURLRoadmap);
const usersConnection = mongoose.createConnection(mongoURLUsers);

module.exports = {
    mainConnection: mongoose.connection,
    roadmapConnection,
    usersConnection
};