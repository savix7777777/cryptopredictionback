const jwt = require('jsonwebtoken');

const secretKey = process.env.JWT_SECRET;

module.exports = (req, res, next) => {
    if (req.method === 'OPTIONS') {
        return next();
    }

    try {
        const token = req.cookies.token;

        if (!token) {
            return res.status(401).json({ message: 'User is not authorized' });
        }

        const decoded = jwt.verify(token, secretKey);
        req.user = decoded;
        next();

    } catch (e) {
        res.status(401).json({ message: 'User is not authorized' });
    }
};