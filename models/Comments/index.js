const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
    author: String,
    authorUsername: String,
    authorId: String,
    text: String,
    date: String,
    period: Number,
    name: String,
    likes: Number,
    dislikes: Number,
    replies: Number,
    sentiment: String,
    repliedToAuthorUsername: String,
    repliedToCommentId: String,
    repliedToPeriod: Number,
});

module.exports = { commentSchema };
