const mongoose = require('mongoose');

const { commentSchema } = require("../Comments");

const cryptoCoinSchema = new mongoose.Schema({
    data1D: {
        price: Number,
        accuracy: Number,
        comments: [commentSchema],
        predictionsHistory: [Number],
    },
    data7D: {
        price: Number,
        accuracy: Number,
        comments: [commentSchema],
        predictionsHistory: [Number],
    },
    data30D: {
        price: Number,
        accuracy: Number,
        comments: [commentSchema],
        predictionsHistory: [Number],
    },
    currentPrice: Number,
    histoDate6: [Number],
    name: String,
    histoDate48: [Number],
    histoDate168: [Number],
    histoDate209: [Number],
    symbol: String,
    icon: String,
    website: String,
    reddit: String,
});

module.exports = { cryptoCoinSchema };
