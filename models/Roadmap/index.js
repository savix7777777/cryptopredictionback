const mongoose = require('mongoose');

const roadmapItemSchema = new mongoose.Schema({
    release: String,
    updatesEN: [String],
    updatesUK: [String],
    updatesDE: [String],
    dislikes: Number,
    likes:  Number
});

const roadmapSchema = new mongoose.Schema({
    roadmap: [roadmapItemSchema]
});

module.exports = { roadmapSchema };

