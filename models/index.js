const { roadmapConnection, usersConnection } = require('../db');

const { roadmapSchema } = require('./Roadmap');
const { userSchema } = require('./User');

const Roadmap = roadmapConnection.model('Roadmap', roadmapSchema, 'roadmap');

const User = usersConnection.model('User', userSchema, 'users');

module.exports = { Roadmap, User };
