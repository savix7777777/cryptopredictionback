const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.delete('/api/delete-comment/:cryptocurrency/:period/:commentId', async (req, res) => {
        const { cryptocurrency, period, commentId } = req.params;


        try {
            const model = mongoose.model(cryptocurrency, cryptoCoinSchema);

            const updateField = `data${period}D.comments`;

            await model.findOneAndUpdate(
                {},
                { $pull: { [updateField]: { _id: commentId } } },
                { new: true }
            );

            res.status(200).json({ message: 'Comment deleted successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}