const mongoose = require('mongoose').default;
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

    module.exports = app => {
        app.get('/api/cryptos/:name', async (req, res) => {
            try {
                const { name } = req.params;

                const model = mongoose.model(name, cryptoCoinSchema, name);
                const data = await model.find({});

                if (!data) {
                    return res.status(404).json({ message: 'Cryptocurrency not found' });
                }

                res.json(data[0]);
            } catch (error) {
                res.status(500).json({ message: error.message });
            }
        });
    }
