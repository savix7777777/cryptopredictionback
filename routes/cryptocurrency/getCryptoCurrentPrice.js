const mongoose = require('mongoose').default;
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.get('/api/cryptos/:name/current-price', async (req, res) => {
        try {
            const { name } = req.params;

            const model = mongoose.model(name, cryptoCoinSchema, name);
            const data = await model.findOne({}, 'currentPrice');

            if (!data) {
                return res.status(404).json({ message: 'Cryptocurrency not found' });
            }

            res.status(200).json({ currentPrice: data.currentPrice });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}
