const mongoose = require('mongoose').default;
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.get('/api/cryptos/:name/histo-date/:period', async (req, res) => {
        try {
            const { name, period } = req.params;

            const model = mongoose.model(name, cryptoCoinSchema, name);
            const data = await model.findOne({}, [`histoDate${period}`]);

            if (!data) {
                return res.status(404).json({ message: 'Cryptocurrency not found' });
            }

            res.status(200).json({ [`histoDate${period}`]: data[`histoDate${period}`] });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}
