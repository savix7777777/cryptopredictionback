const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");
const { determineSentiment } = require("../../utils/determineSentiment");

module.exports = app => {
    app.post('/api/add-comment', async (req, res) => {
        const { cryptocurrency, comment } = req.body;

        comment.sentiment = await determineSentiment(comment.text);

        try {
            const model = mongoose.model(cryptocurrency, cryptoCoinSchema);

            if (comment.repliedToCommentId && comment.repliedToAuthorUsername) {
                const repliedCommentUpdateField = `data${comment.repliedToPeriod}D.comments`;
                await model.findOneAndUpdate(
                    { [`${repliedCommentUpdateField}._id`]: comment.repliedToCommentId },
                    { $inc: { [`${repliedCommentUpdateField}.$.replies`]: 1 } }
                );
            }

            const updateField = `data${comment.period}D.comments`;

            await model.findOneAndUpdate(
                {},
                { $push: { [updateField]: comment } },
                { new: true }
            );

            res.status(200).json({ message: 'Comment added successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}