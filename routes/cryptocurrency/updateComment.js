const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");
const { determineSentiment } = require("../../utils/determineSentiment");

module.exports = app => {
    app.put('/api/update-comment/:cryptocurrency/:period/:commentId', async (req, res) => {
        const { cryptocurrency, period, commentId } = req.params;
        const { newText } = req.body.body;

        const sentiment = await determineSentiment(newText);

        try {
            const model = mongoose.model(cryptocurrency, cryptoCoinSchema);

            const updateField = `data${period}D.comments`;

            await model.findOneAndUpdate(
                { [`${updateField}._id`]: commentId },
                { $set: {
                    [`${updateField}.$.text`]: newText,
                        [`${updateField}.$.sentiment`]: sentiment,
                }},
                { new: true }
            );

            res.status(200).json({ message: 'Comment updated successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}