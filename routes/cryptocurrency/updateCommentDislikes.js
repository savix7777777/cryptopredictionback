const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.put('/api/update-comment-dislikes/:cryptocurrency/:period/:commentId', async (req, res) => {
        const { cryptocurrency, period, commentId } = req.params;
        const { delta } = req.body.body;

        if (![1, -1].includes(delta)) {
            return res.status(400).json({ message: 'Invalid delta value' });
        }

        try {
            const model = mongoose.model(cryptocurrency, cryptoCoinSchema);

            const updateField = `data${period}D.comments`;

            await model.findOneAndUpdate(
                { [`${updateField}._id`]: commentId },
                { $inc: { [`${updateField}.$.dislikes`]: delta } },
                { new: true }
            );

            res.status(200).json({ message: 'Dislikes count updated successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}
