const mongoose = require("mongoose");
const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.put('/api/update-comment-sentiment/:cryptocurrency/:period/:commentId', async (req, res) => {
        const { cryptocurrency, period, commentId } = req.params;
        const { sentiment } = req.body.body;

        try {
            const model = mongoose.model(cryptocurrency, cryptoCoinSchema);

            const updateField = `data${period}D.comments`;

            await model.findOneAndUpdate(
                { [`${updateField}._id`]: commentId },
                { $set: { [`${updateField}.$.sentiment`]: sentiment }},
                { new: true }
            );

            res.status(200).json({ message: 'Comment sentiment updated successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}