const mongoose = require('mongoose').default;

const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.get('/api/comments', async (req, res) => {
        try {
            const collections = await mongoose.connection.db.listCollections().toArray();
            const allComments = [];

            for (const collection of collections) {
                const model = mongoose.model(collection.name, cryptoCoinSchema, collection.name);

                const docs = await model.find({}, { 'data1D.comments': 1, 'data7D.comments': 1, 'data30D.comments': 1 });

                allComments.push(...docs.reduce((accum, doc) => {
                    return accum.concat(doc.data1D.comments, doc.data7D.comments, doc.data30D.comments);
                }, []));
            }

            res.status(200).send({ "comments": allComments });
        } catch (err) {
            console.log(err);
            res.status(500).send(err);
        }
    });
}
