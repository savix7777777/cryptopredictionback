const { usersConnection } = require("../../db");
const mongoose = require("mongoose");

module.exports = app => {
    app.get('/api/usersAmount', async (req, res) => {
        try {
            const userModel = usersConnection.model('User', new mongoose.Schema({}), 'users');

            const usersCount = await userModel.countDocuments({});

            res.status(200).send({ "usersAmount": usersCount });
        } catch (err) {
            console.log(err);
            res.status(500).send(err);
        }
    });
}