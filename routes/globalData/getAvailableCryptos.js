const mongoose = require('mongoose').default;

module.exports = app => {
    app.get('/api/cryptos', async (req,res) => {
        await mongoose.connection.db.listCollections().toArray()
            .then(cryptos => {
                cryptos = cryptos.map(item => item.name);
                res.status(200).send(cryptos);
            })
            .catch(err => {
                console.log(err);
                res.status(500).send({ message: 'Server error: ', err });
            });
    });
}
