const mongoose = require('mongoose').default;

const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.get('/api/cryptos-list', async (req, res) => {
        try {
            const collections = await mongoose.connection.db.listCollections().toArray();
            let cryptocurrencies = [];

            for (let collection of collections) {
                const model = mongoose.model(collection.name, cryptoCoinSchema, collection.name);

                const doc = await model.findOne({}, {
                    'data1D.price': 1,
                    'data7D.price': 1,
                    'data30D.price': 1,
                    'data1D.accuracy': 1,
                    'data7D.accuracy': 1,
                    'data30D.accuracy': 1,
                    'symbol': 1,
                    'icon': 1
                }).sort({_id: -1});

                if (doc) {
                    cryptocurrencies.push({
                        name: collection.name,
                        symbol: doc.symbol,
                        icon: doc.icon,
                        data1D: {
                            price: doc.data1D.price,
                            accuracy: doc.data1D.accuracy
                        },
                        data7D: {
                            price: doc.data7D.price,
                            accuracy: doc.data7D.accuracy
                        },
                        data30D: {
                            price: doc.data30D.price,
                            accuracy: doc.data30D.accuracy
                        }
                    });
                }
            }

            cryptocurrencies.sort((first, second) => second.data1D.price - first.data1D.price);

            res.json(cryptocurrencies);
        } catch (error) {
            res.status(500).json({message: error.message});
        }
    });
}