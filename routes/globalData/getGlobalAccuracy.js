const mongoose = require('mongoose').default;

const { cryptoCoinSchema } = require("../../models/CryptoCoin");

module.exports = app => {
    app.get('/api/accuracy', async (req, res) => {
        try {
            const collections = await mongoose.connection.db.listCollections().toArray();
            let allAccuracies;

            for (const collection of collections) {
                const model = mongoose.model(collection.name, cryptoCoinSchema, collection.name);

                const docs = await model.find({}, { 'data1D.accuracy': 1, 'data7D.accuracy': 1, 'data30D.accuracy': 1 });

                allAccuracies = docs.reduce((accum, doc) =>
                    accum.concat(doc.data1D.accuracy, doc.data7D.accuracy, doc.data30D.accuracy), []);
            }

            let validateLength = allAccuracies.length;

            const meanAccuracy = allAccuracies.reduce((accum, number) => {
                if (number) {
                    return accum + number;
                } else {
                    validateLength -= 1;
                    return accum;
                }
            }, 0) / validateLength;

            res.status(200).send({ "globalAccuracy": meanAccuracy.toFixed(1) + "%" });
        } catch (err) {
            console.log(err);
            res.status(500).send(err);
        }
    });
}
