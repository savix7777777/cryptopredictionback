const { Roadmap } = require("../../models");

module.exports = app => {
    app.get('/api/roadmap', async (req, res) => {
        try {
            const roadmaps = await Roadmap.find();
            res.status(200).send(roadmaps);
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: 'Server error: ', err });
        }
    });
}