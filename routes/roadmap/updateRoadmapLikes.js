const { Roadmap } = require("../../models");

module.exports = app => {
    app.put('/api/update-roadmap-likes/:roadmapItemId', async (req, res) => {
        const { roadmapItemId } = req.params;
        const { delta } = req.body.body;

        if (![1, -1].includes(delta)) {
            return res.status(400).json({ message: 'Invalid delta value' });
        }

        try {
            const roadmapDoc = await Roadmap.findOne();

            if (!roadmapDoc) {
                return res.status(404).json({ message: 'Roadmap not found' });
            }

            const item = roadmapDoc.roadmap.id(roadmapItemId);

            if (!item) {
                return res.status(404).json({ message: 'Roadmap item not found' });
            }

            item.likes += delta;
            await roadmapDoc.save();

            res.status(200).json({ message: 'Roadmap item likes count updated successfully' });
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    });
}
