const { User } = require('../../models');
const verifyToken = require('../../middlewares/jwtCheck');

module.exports = app => {
    app.get('/api/auto-login', verifyToken, async (req, res) => {
        try {
            const userId = req.user.userId;

            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).send({ message: 'User not found.' });
            }

            res.status(200).send({
                message: "User authenticated.",
                user: {
                    id: user._id,
                    name: user.name,
                    username: user.username,
                    watchlist: user.watchlist,
                    subscribes: user.subscribes,
                    likes: user.likes,
                    dislikes: user.dislikes,
                }
            });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: 'Server error: ', err });
        }
    });
};

