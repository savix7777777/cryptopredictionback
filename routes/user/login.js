const jwt = require('jsonwebtoken');
const { User } = require('../../models');

const secretKey = process.env.JWT_SECRET;

module.exports = app => {
    app.post('/api/login', async (req, res) => {
        try {
            const { username, password } = req.body;

            const user = await User.findOne({
                $or: [
                    { username: username },
                    { email: username }
                ]
            });
            if (!user) {
                return res.status(400).send({ message: 'Invalid username or password.' });
            }

            const isMatch = await user.comparePassword(password);
            if (!isMatch) {
                return res.status(400).send({ message: 'Invalid username or password.' });
            }

            const token = jwt.sign({ userId: user._id }, secretKey, { expiresIn: '7d' });

            res.cookie('token', token, {
                httpOnly: true,
                maxAge: 7 * 24 * 60 * 60 * 1000,
                secure: process.env.MODE !== 'dev',
                sameSite: process.env.MODE === 'dev' ? 'lax' : 'strict',
            });

            res.status(200).send({
                message: "Login successful.",
                user: {
                    id: user._id,
                    name: user.name,
                    username: user.username,
                    watchlist: user.watchlist,
                    subscribes: user.subscribes,
                    likes: user.likes,
                    dislikes: user.dislikes,
                }
            });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: 'Server error: ', err });
        }
    });
};
