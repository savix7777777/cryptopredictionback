module.exports = app => {
    app.get('/api/logout', (req, res) => {
        try {
            res.clearCookie('token', {
                httpOnly: true,
                secure: process.env.MODE !== 'dev',
                sameSite: process.env.MODE === 'dev' ? 'lax' : 'strict',
            });

            res.status(200).send({
                message: "Logout successful."
            });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: 'Server error: ', err });
        }
    });
};