const { User } = require('../../models');

module.exports = app => {
    app.post('/api/register', async (req, res) => {
        try {
            const { name, username, email, password } = req.body;

            const existingUser = await User.findOne({
                $or: [
                    { username: username },
                    { email: email }
                ]
            });

            if (existingUser) {
                if (existingUser.username === username) {
                    return res.status(400).send({ message: 'Username is already taken.' });
                }
                if (existingUser.email === email) {
                    return res.status(400).send({ message: 'Email is already registered.' });
                }
            }

            const user = new User({ name, username, email, password });
            await user.save();

            res.status(200).send({ message: "Registration successful." });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: 'Server error: ', err });
        }
    });
};
