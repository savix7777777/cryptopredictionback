const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");

const { User } = require('../../models');

const secretKey = process.env.JWT_SECRET;

module.exports = app => {
    app.post('/api/reset-password', async (req, res) => {
        try {
            const user = await User.findOne({ email: req.body.email });

            if (!user) {
                return res.status(404).send("No user with that email found");
            }

            const resetToken = jwt.sign({userId: user._id}, secretKey, {expiresIn: '1h'});

            let transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: process.env.EMAIL,
                    pass: process.env.EMAIL_PASSWORD,
                }
            });

            let mailOptions = {
                from: process.env.EMAIL,
                to: user.email,
                subject: 'Password Reset link',
                text: `Click this link to reset your password: http://localhost:3000/reset-password/${resetToken}`
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.error("Error sending email", error);
                    return res.status(500).send("Error sending email");
                } else {
                    console.log("Email sent: " + info.response);
                    return res.status(200).send("Email sent");
                }
            });
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: 'Server error: ', err });
        }
    });
};
