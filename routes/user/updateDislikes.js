const { User } = require('../../models');

module.exports = app => {
    app.post('/api/dislikes', async (req, res) => {
        const { dislikeObjId, userId } = req.body;

        if (!dislikeObjId || typeof dislikeObjId !== 'string') {
            return res.status(400).send({ message: 'Invalid item.' });
        }

        let rateConflict = false;

        try {
            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).send({ message: 'User not found.' });
            }

            if (user.dislikes.includes(dislikeObjId)) {
                const index = user.dislikes.indexOf(dislikeObjId);
                user.dislikes.splice(index, 1);
            } else {
                user.dislikes.push(dislikeObjId);
                const likeIndex = user.likes.indexOf(dislikeObjId);
                if (likeIndex !== -1) {
                    rateConflict = true;
                    user.likes.splice(likeIndex, 1);
                }
            }

            await user.save();

            res.status(200).send({ likes: user.likes, dislikes: user.dislikes, rateConflict });
        } catch (error) {
            console.error(error);
            res.status(500).send({ message: 'Internal server error.' });
        }
    });
};
