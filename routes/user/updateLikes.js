const { User } = require('../../models');

module.exports = app => {
    app.post('/api/likes', async (req, res) => {
        const { likeObjId, userId } = req.body;

        if (!likeObjId || typeof likeObjId !== 'string') {
            return res.status(400).send({ message: 'Invalid item.' });
        }

        let rateConflict = false;

        try {
            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).send({ message: 'User not found.' });
            }

            if (user.likes.includes(likeObjId)) {
                const index = user.likes.indexOf(likeObjId);
                user.likes.splice(index, 1);
            } else {
                user.likes.push(likeObjId);
                const dislikeIndex = user.dislikes.indexOf(likeObjId);
                if (dislikeIndex !== -1) {
                    rateConflict = true;
                    user.dislikes.splice(dislikeIndex, 1);
                }
            }

            await user.save();

            res.status(200).send({ likes: user.likes, dislikes: user.dislikes, rateConflict });
        } catch (error) {
            console.error(error);
            res.status(500).send({ message: 'Internal server error.' });
        }
    });
};
