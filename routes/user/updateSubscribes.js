const { User } = require('../../models');

module.exports = app => {
    app.post('/api/subscribes', async (req, res) => {
        const { crypto, userId } = req.body;

        if (!crypto || typeof crypto !== 'string') {
            return res.status(400).send({ message: 'Invalid item.' });
        }

        try {
            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).send({ message: 'User not found.' });
            }

            if (user.subscribes.includes(crypto)) {
                const index = user.subscribes.indexOf(crypto);
                user.subscribes.splice(index, 1);
            } else {
                user.subscribes.push(crypto);
            }

            await user.save();

            res.status(200).send({ subscribes: user.subscribes });
        } catch (error) {
            console.error(error);
            res.status(500).send({ message: 'Internal server error.' });
        }
    });
};
