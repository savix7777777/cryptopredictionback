const { User } = require('../../models');

module.exports = app => {
    app.post('/api/watchlist', async (req, res) => {
        const { crypto, userId } = req.body;

        if (!crypto || typeof crypto !== 'string') {
            return res.status(400).send({ message: 'Invalid item.' });
        }

        try {
            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).send({ message: 'User not found.' });
            }

            if (user.watchlist.includes(crypto)) {
                const index = user.watchlist.indexOf(crypto);
                user.watchlist.splice(index, 1);
            } else {
                user.watchlist.push(crypto);
            }

            await user.save();

            res.status(200).send({ watchlist: user.watchlist });
        } catch (error) {
            console.error(error);
            res.status(500).send({ message: 'Internal server error.' });
        }
    });
};
