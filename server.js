const express = require('express');
require('dotenv').config();
const cors = require('cors');
const cookieParser = require('cookie-parser');

const routes = require("./routes");
const { port } = require("./config");
const { runCronJobs } = require("./cron");
const { openWebsockets } = require("./websocket");

require('./db');

const app = express();

app.use(cors({
    origin: (origin, callback) => {
        if (!origin || origin === 'https://coinprophetapp.com' || origin.startsWith('http://localhost:')) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    },
    credentials: true
}));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());

function initializeApp() {
    runCronJobs().then(r => r);
    openWebsockets().then(r => r);
    routes(app);
    app.listen(port, () => {
        console.log('Connect success!');
    });
}

initializeApp();

module.exports = app;