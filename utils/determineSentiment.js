const { PythonShell } = require("python-shell");

const determineSentiment = async (text) => {
    let options = {
        mode: 'text',
        pythonOptions: ['-u'],
        cwd: process.env.MODE === 'dev' ? '../CryptoPredictorML' : '/home/ubuntu/cryptoprediction',
        pythonPath: process.env.MODE === 'dev' ? 'venv/bin/python3' : '/home/ubuntu/cryptoprediction/venv/bin/python3',
        args: [text]
    };
    try {
        const result =  await PythonShell.run('CommentsSentiment.py', options);
        return result[0];
    } catch (err) {
        console.error(err)
    }
}

module.exports = { determineSentiment }
