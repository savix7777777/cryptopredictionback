const mongoose = require("mongoose");
const WebSocket = require("ws");

const { mainConnection } = require('../db');
const { cryptoCoinSchema } = require("../models/CryptoCoin");

const createSubscriptionRequest = (fromSymbol, toSymbol) => {
    return JSON.stringify({
        "action": "SubAdd",
        "subs": [`5~CCCAGG~${fromSymbol}~${toSymbol}`]
    });
};

const sendData = async (fromSymbol, toSymbol, message, cryptosArray) => {
    const name = cryptosArray.find((crypto) => crypto.symbol === fromSymbol).name;
    try {
        const model = mongoose.model(name, cryptoCoinSchema, name);
        const result = await model.findOneAndUpdate(
            { },
            {
                $set: {
                    'currentPrice': message.PRICE,
                },
            },
            { new: true }
        );

        console.log(name + " current price updated successfully", result);
    } catch (error) {
        console.log("Error updating 1D prediction: ", error);
    }
};

const getCryptoSymbols = async () => {
    const cryptosArray = [];
    const collections = await mainConnection.db.listCollections().toArray();
    for (let collection of collections) {
        const collectionName = collection.name;
        const Crypto = mongoose.model(collectionName, cryptoCoinSchema, collectionName);
        const doc = await Crypto.findOne({}, 'symbol');
        if (doc && doc.symbol) {
            cryptosArray.push({
                name: collectionName,
                symbol: doc.symbol,
            });
        }
    }
    return cryptosArray;
};

const openWebsockets = async () => {
    try {
        mainConnection.on('connected', async () => {
            let accountState = 0;
            const cryptosArray = await getCryptoSymbols();

            const connectWebSocket = async () => {
                const CRYPTO_COMPARE_WS_URL = `wss://streamer.cryptocompare.com/v2?api_key=${accountState ? process.env.CRYPTO_COMPARE_KEY_1 : process.env.CRYPTO_COMPARE_KEY_0}`;
                const ws = new WebSocket(CRYPTO_COMPARE_WS_URL);
                const lastMessageTimestamps = {};

                ws.on('open', async () => {
                    cryptosArray.forEach(crypto => {
                        const subRequest = createSubscriptionRequest(crypto.symbol, 'USD');
                        console.log('Subscription for ' + crypto.name + ' done');
                        ws.send(subRequest);
                    });
                });

                ws.on('message', (data) => {
                    const message = JSON.parse(data.toString());

                    if (message.TYPE === '5' && message.FROMSYMBOL && message.TOSYMBOL) {
                        const fromSymbol = message.FROMSYMBOL;
                        const toSymbol = message.TOSYMBOL;

                        const key = fromSymbol + toSymbol;

                        const currentTime = new Date().getTime();
                        const lastMessageTime = lastMessageTimestamps[key] || 0;

                        if (currentTime - lastMessageTime > 5000) {
                            sendData(fromSymbol, toSymbol, message, cryptosArray);
                            lastMessageTimestamps[key] = currentTime;
                        }
                    }
                });

                ws.on('error', (error) => {
                    console.error('WebSocket error:', error);
                });

                ws.on('close', (code, reason) => {
                    console.log(`WebSocket connection closed with code: ${code}, reason: ${reason}`);
                    if (accountState) accountState = 0;
                    else accountState = 1;
                    setTimeout(connectWebSocket, 70000);
                });
            }
            await connectWebSocket();
        });
    } catch (err) {
        console.error(err);
    }
};

module.exports = { openWebsockets };